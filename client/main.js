// I M P O R T S
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';
import { Accounts } from 'meteor/accounts-base';
import './main.html';

// D E F I N E S
MASK = '<small>&#x1F435;</small>'; //░

// new Array method
Object.defineProperty(Array.prototype, 'piecesOf', {value: function(n) {
    return Array.from(Array(Math.ceil(this.length/n)), (_,i)=>this.slice(i*n,i*n+n));
}});

Accounts.ui.config({
  passwordSignupFields: 'USERNAME_AND_EMAIL',
});

interval = {}; 

// U T I L S

// timer 
var countdown = function() {
  TIMER = Session.get('time');
  if (TIMER > 0) {
    TIMER--;
    Session.set("time", TIMER);
    return TIMER;
  } else {
    swal({ title: Session.get('total'), text: "Sorry too slow.", imageUrl: "chimp.png"});
    newGame();
  }
};

// capitalise
var initcaps = function (string) {
  return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
}

// some ranking algo
function hot(ups,downs,date){
  var score = ups - downs;
  var order = log10(Math.max(Math.abs(score), 1));
  var sign = score>0 ? 1 : score<0 ? -1 : 0;
  var seconds = epochSeconds(date) - 1134028003;
  var product = order + sign * seconds / 45000;
  return Math.round(product*10000000)/10000000;
}
function log10(val){
  return Math.log(val) / Math.LN10;
}
function epochSeconds(d){
  return (d.getTime() - new Date(1970,1,1).getTime())/1000;
}

// new game
var newGame = function() {

  Session.set("round", 0);
  Session.set("total", 0);
  Session.set("index", 1);
  Session.set("round", 1);

  Meteor.clearInterval(interval);
  interval = Meteor.setInterval(countdown, 1000); // countdown in seconds     

  return ;
}

// new round
var newRound = function() {

  Session.set("round", Session.get("round") + 1);
  Session.set("index", 1);

  Meteor.clearInterval(interval);
  interval = Meteor.setInterval(countdown, 1000); // countdown in seconds     

}

// update totals
var updateTotals = function() {

  currentUser.profile.ticks += (10 - Session.get("time"));
  currentUser.profile.best = Math.max(currentUser.profile.best, Session.get("total"));
  currentUser.profile.last = Session.get("total");
  Meteor.users.update(Meteor.userId(), {$set: {"profile": currentUser.profile }});

}

var rankUser = function(){

  var high = currentUser.profile.high||0; time = currentUser.profile.ticks||0, join = new Date(); 
  var saws = currentUser.profile.high + currentUser.profile.last + currentUser.profile.best;
  currentUser.profile.gold = Math.round(Math.abs(((new Date(2013, 1, 17)).getTime()-(join).getTime())/(24*60*60*1000) ))*(Math.round(saws/time));
  currentUser.profile.rank = hot(saws,time,join);

  Meteor.users.update(Meteor.userId(), {$set: {"profile": currentUser.profile }});

}

// H E L P E R S
Template.registerHelper(
  'equals', function(v1, v2) {
    return (v1 === v2);
  },
);

Template.app.rendered = function(){    
  $('#login-name-link').hide();    
  if (Meteor.user()) {
    Session.set("currentPage", "game");
    Accounts._loginButtonsSession.set('dropdownVisible', false);
  } else {
    Session.set("currentPage", "login");
    Accounts._loginButtonsSession.set('dropdownVisible', true);
  }
}

Template.app.onCreated(function gameOnCreated() {

  Session.set("columns", 9);
  Session.set("round", 1);
  Session.set("index", 1);
  Session.set("total", 0);

});

Template.app.helpers({
  currentPage() {
    return Session.get("currentPage");
  },
  gold() {
    currentUser = Meteor.user();
    if (currentUser) {
      var high = currentUser.profile.high + 1; time = currentUser.profile.ticks, join = new Date();
      var saws = currentUser.profile.high + currentUser.profile.last + currentUser.profile.best;
      var feO2 = Math.round(Math.abs(((new Date(2013, 1, 17)).getTime()-(join).getTime())/(24*60*60*1000) ))*(Math.round(saws/time));
      return feO2;
    } else {
      return "";
    }
  },
});

Template.menu.helpers({
  initcaps(string) {
    return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
  },
  currentPageName(index) {
    pages = {'leaderboard': 'Leaderboard', 'profile': 'Profile', 'login': 'Sign In'};
    $(".topnav li a").show();
    $("."+index).hide();
    return pages[index];
  }
});

Template.profile.helpers({
  initcaps(string) {
    return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
  },
  profile() {    
    var form = [];
    var field = {};

    $.each(Meteor.user().profile, function (key, value) {

      field.id = key;
      field.type = typeof(value)=='string' ? 'text' : typeof(value);
      field.type = typeof(value)=='number' ? typeof(value) : field.type;
      field.type = typeof(value.getMonth) === 'function' ? 'date' : field.type;
      field.type = (key.toLowerCase().includes("email")) ? 'email' : field.type ; 

      field.placeholder = key;
      field.value = value;
      if (field.type == 'date'){
        field.text = moment(value, "YYYY-MM-DD").format("DD MMMM YYYY")
      }
      form.push(field);
      field = {};  
    });

    return form;
  },
});

Template.leaderboard.helpers({
  leaderboard() {
    leaders = Meteor.users.find({}, { sort: { 'profile.rank' : -1}, limit: 10 }).fetch();
    return leaders;
  },
});

Template.game.rendered = function(){    
  //$('.nthcol a').hide();
};

Template.game.helpers({
  best() {   
    currentUser = Meteor.user();
    if (currentUser) {
      return currentUser.profile.best
    } else {
      return "";
    }
  },
  high() {   
    currentUser = Meteor.user();
    if (currentUser) {
      return currentUser.profile.high
    } else {
      return "";
    }
  },
  last() {   
    currentUser = Meteor.user();
    if (currentUser) {
      return currentUser.profile.last
    } else {
      return "";
    }
  },
  round() {
    return Session.get("round");
  },
  total() {
    return Session.get("total");
  },
  time() {
    return Session.get("time");
  },
  puzzle() {
    Session.set("time", 10);

    var round = Session.get("round");
    var board = 48;
    var columns = _.random(Math.min(8, 5 + Math.floor(round/5)), Math.min(9, 5 + Math.floor(round/2)));

    Session.set("columns", columns);

    // generate array
    var list = [];
    for (var i = 1; i <= board; i++) {
        if (i > 0 && i < columns) {
          list.push(i);
        } else {
          list.push(0);
        }
    }
    list = list.sort(function(a, b){return 0.5 - Math.random()});   

    // render table
    var arr = list.piecesOf(8);
    var body, tab, tr, td, tn, row, col;
    body = document.getElementsByTagName('body')[0];
    tab = document.createElement('table');
    tab.setAttribute("id", "puzzle");

    var fullscreen = Session.get('fullscreen');
    if (fullscreen){
      tab.className += "fullscreen";
    }

    for (row=0; row < arr.length; row++){
        tr = document.createElement('tr');
        for (col=0; col < arr[row].length; col++){
            td = document.createElement('td');
            if (arr[row][col]==0){
              data = ' '; // seems about the right width
            } else {
              td.className += "nthcol";
              data = arr[row][col];
            }
            cell = "<a href='#' id='"+data+"'>"+data+"</a>"
            td.innerHTML = cell;
            tr.appendChild(td);
        }
        tab.appendChild(tr);
    }
    return tab.outerHTML;
  }
});

Template.body.events({
  'click .newgame'(event, instance) {
    event.preventDefault();
    newGame();
    Session.set('currentPage', 'game');
  },
  'click .profile'(event, instance) {
    event.preventDefault();
    Session.set('currentPage', 'profile');    
  },  
  'click .leaderboard'(event, instance) {
    event.preventDefault();
    Session.set('currentPage', 'leaderboard');
  },  
  'click .logout'(event, instance){
    Meteor.logout();
    Accounts._loginButtonsSession.set('dropdownVisible', true);
    Session.set('currentPage', 'login');
  },
  'click .login'(event, instance) {
    event.preventDefault();
    Accounts._loginButtonsSession.set('dropdownVisible', true);
    Accounts._loginButtonsSession.set('inForgotPasswordFlow', false);
    Session.set('currentPage', 'login');
  },
  'click .changePassword'(event, instance) {
    event.preventDefault();
    Accounts._loginButtonsSession.set('dropdownVisible', true);
    Accounts._loginButtonsSession.set('inChangePasswordFlow', true);
    Session.set('currentPage', 'login');
  },
  'click .forgotPassword'(event, instance) {
    event.preventDefault();
    Session.set('currentPage', 'login');
    Accounts._loginButtonsSession.set('dropdownVisible', true);
    Accounts._loginButtonsSession.set('inForgotPasswordFlow', true);
  },
  'click .toggleFull'(event, instance) {
    Session.set('fullscreen', 1-(Session.get('fullscreen')||0));
    $('.close').toggle();    
  },
  'click #menu' (event, instance) {
    var x = document.getElementById("menu");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
  }
})

Template.game.events({  
  'click .nthcol a'(event, instance) {
    event.preventDefault();
    currentNumber = parseInt(event.currentTarget.id);
    currentUser = Meteor.user();
    
    // clicks on right number
    if (currentNumber == Session.get("index")){
      Session.set("total", (Session.get("total") + Session.get("index")));

      // right number is last
      if (currentNumber == Session.get("columns")-1) {
        updateTotals();
        newRound();
      } else { 
        // mask numbers and start timer 
        if (currentNumber == 1) {
          $('.nthcol a').html(MASK); 
        }
        // next number
        $('#'+event.currentTarget.id).html(' ');
        Session.set("index", Session.get("index") + 1);
      }
      currentUser.profile.high += 1;
      rankUser(); // add gold/rank/best/last
    } else {
      // wrong number
      swal({ title: "Scored: "+Session.get('total'), text: "You clicked "+currentNumber, imageUrl: "chimp.png" });
      Meteor.clearInterval(interval);
    }
  },
});

Template.profile.events({
  'change .field'(event, instance) {
    var field = event.target;
    var value = $(field).val();

    if (field.type == 'number' ) {
      value = parseInt(value);
    }
    if (field.type == 'date' ) {
      $(field).attr("data-date", moment(value, "YYYY-MM-DD").format($(field).attr("data-date-format")))
      value = moment(value, "YYYY-MM-DD" ).toDate();
    }
    
    currentUser.profile[field.id] = value;
    Meteor.users.update(Meteor.userId(), {$set: {"profile": currentUser.profile }});
  },
});
