import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

// some ranking algo
function hot(ups,downs,date){
  var score = ups - downs;
  var order = log10(Math.max(Math.abs(score), 1));
  var sign = score>0 ? 1 : score<0 ? -1 : 0;
  var seconds = epochSeconds(date) - 1134028003;
  var product = order + sign * seconds / 45000;
  return Math.round(product*10000000)/10000000;
}
function log10(val){
  return Math.log(val) / Math.LN10;
}
function epochSeconds(d){
  return (d.getTime() - new Date(1970,1,1).getTime())/1000;
}

Accounts.config({
  sendVerificationEmail: true,
  forbidClientAccountCreation: false
});

Meteor.publish(null, function() {
  return Meteor.users.find({}, {fields: {username: 1, name: 1, profile: 1}});
  //return Meteor.users.find({_id: this.userId}, {fields: {isAdmin: 1, name: 1, groups: 1}});
});  

///  T R I G G E R S  ==================================================
Accounts.onCreateUser(function(options, user) {
    
    if (user.username == 0) {
      user.username = user.emails[0].address.split('@')[0].replace('.','');
    }
    
    var userProperties = {
      profile: options.profile || {},
      isAdmin: user.isAdmin,
      groups: ['world', user.username]
    };
    
    user = _.extend(user, userProperties);

    if (options.email) {
      user.profile.email = options.email;
    }
    
    if (!user.profile.name) {
      user.profile.name = user.username;
    }
        
    if (!Meteor.users.find().count() ) {
      user.isAdmin = true;
    }
  
    if (options.profile) {
      user.profile = options.profile;
    }
    
    user.profile.best = 0;
    user.profile.last = 0;
    user.profile.high = 0;
    user.profile.rank = 0;
    user.profile.gold = 0;
    user.profile.ticks = 0;    
    user.profile.joined = new Date();

    return user;
    
});

///  S E R V I C E S   =================================================
Meteor.methods({
  'version': function(){
    console.log("gochimp 0.09");
  },
  'setAdmin': function(user, isAdmin){
    Meteor.users.update({_id: user._id}, {$set: { 'isAdmin': isAdmin } });
        
    console.log(isAdmin);
    
  },
  'cleanUsers': function(){

    users = Meteor.users.find({}).fetch();
    _.each(users, function (user){
      if (user.profile.gold == "NaN"){
        
        user.profile.high = 1;
        user.profile.last = 1;
        user.profile.rank = 0;
        user.profile.gold = 0;
        user.profile.joined = new Date();

        Meteor.users.update({_id: user._id}, {$set: {"profile": user.profile }});
      }
    })

    console.log("reset all users");
        
  },
  'resetUsers': function(){

    users = Meteor.users.find({}).fetch();
    _.each(users, function (user){
      user.profile.best = 0;
      user.profile.last = 0;
      user.profile.high = 0;
      user.profile.rank = 0;
      user.profile.gold = 0;
      user.profile.ticks = 0;    
      user.profile.joined = new Date();

      Meteor.users.update({_id: user._id}, {$set: {"profile": user.profile }});
    })

    console.log("reset all users");
    
  },
  'rankUsers': function(){

    users = Meteor.users.find({}).fetch();
    _.each(users, function (currentUser){

      var high = currentUser.profile.high||0; time = currentUser.profile.ticks||0, join = new Date(); 
      var saws = currentUser.profile.high + currentUser.profile.last + currentUser.profile.best;
      currentUser.profile.gold = Math.round(Math.abs(((new Date(2013, 1, 17)).getTime()-(join).getTime())/(24*60*60*1000) ))*(Math.round(saws/time));
      currentUser.profile.rank = hot(saws,time,join); 

      Meteor.users.update({_id: currentUser._id}, {$set: {"profile": currentUser.profile }});
    })
        
    console.log("set all users");
    
  },  
});

Meteor.startup(() => {
  // code to run on server at startup  
});

